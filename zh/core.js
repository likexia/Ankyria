//1.汉化杂项
var cnItems = {
    _OTHER_: [],

    //未分类：
    'Skip>>': '跳过',
    'Game saved!': '游戏已保存！',
    'Copy the following text': '复制以下文本',
    'Exploration': '探索',
    'Shuttle': '穿梭',
    'You wake up to an incessant beeping... it is coming from a bank of monitors': '你在不停的哔哔声中醒来……这声音来自一组监视器',
    'Stumbling out of bed, you go to turn it off. As you do, you notice where you are-': '你跌跌撞撞地从床上爬起来，想要去关掉它。当你这样做的时候，你才注意到你在哪里——',
    'Not enough resources for your current job!': '没有足够的资源来完成当前的工作！',
    'Import': '导入',
    'Workshop': '工作台',
    'Wood': '木头',
    'Water': '水',
    'Paste into the following box': '粘贴到下面的框中',
    'Log': '日志',
    'Jobs': '工作',
    'High up in orbit around a planet': '在环绕行星的轨道上',
    'Deciphering the flashing lights tells you that your orbit is decaying': '通过破译闪烁的灯光，你发现你的轨道正在降低',
    'Continue>>': '继续>>',
    'Buildings': '建筑',
    '/sec': '/秒',
    'Full changelog': '完整更新日志',
    'License': '许可',
    'Resources': '资源',
    'Spear': '矛',
    'East': '东',
    'Deep forest': '森林深处',
    'South': '南',
    'Water bowl': '水碗',
    'Water bowl  (': '水碗 （',
    'Water collecting': '水收集',
    'watercap': '水上限',
    'West': '西',
    'Woodcutting': '伐木',
    'Time': '时间',
    'Rain collecter  (': '雨水收集器  (',
    'Plains': '平原',
    'North': '北',
    'More plains': '更多的平原',
    'Jungle': '丛林',
    'Forest': '森林',
    'Cost': '成本',
    'A massive plain lies this way, stretching to the horizon': '一个巨大的平原就是这样，延伸到地平线',
    'Collect some water from the lake': '从湖里收集一些水',
    'Cut down some of the small trees in the forest': '砍伐森林里的一些小树',
    'Cut down some of the trees in the forest': '砍伐森林里的一些树木',
    'Even better WS': '更好的手杖',
    'Explore deeper in the forest': '在森林深处探索',
    'Explored': '已探索',
    'Larger woodcutting': '大量伐木',
    'Home sweet home': '家，甜蜜的家',
    'Leaf collecter': '叶子收集',
    'Spear making': '制矛',
    'Get': '获得',
    'Make a spear for hunting': '为狩猎做矛',
    'Wooden hatchet': '木斧',
    'Try to chop wood into the perfect walking stick.': '试着把木头砍成完美的手杖。',
    'There are some weird animals on the horizon. Perhaps you could hunt them...?': '地平线上有一些奇怪的动物。也许你可以猎杀它们……?',
    'Make lightweight spears for faster hunting': '制作重量较轻的矛，以便更快地狩猎',
    'Make a wooden hatchet to make the going just that little bit easier': '做一把木制的斧头，让事情变得简单一点',
    'Make a spear for hunting': '做一支狩猎用的矛',
    'Lightweight spears': '更轻的矛',
    'Leaves make building rain collecters faster and improves collection area': '树叶使建筑雨水收集器更快，提高了收集面积',
    'Go exploring in the jungle': '去丛林探险',
    'Go exploring in the forest': '去森林里探险',
    'Explore the area around you to the east': '向东探索你周围的区域',
    'Explore the area around you to the south': '向南探索你周围的区域',
    'Explore the area around you to the west': '向西探索你周围的区域',
    'Explore the area around you to the north.': '向北探索你周围的区域',
    'Change theme': '切换主题',
    'Delete save': '删除存档',
    'Export': '导出',
    'Save': '保存',
    'Gain storage for water': '获得水的存储上限',
    'Shed  (': '小屋  (',
    'Storage shed for resources': '储存资源的棚子',
    'When it rains, collect some water': '下雨时，收集一些水',
    'Cancel current job [This is a button]': '取消当前作业[点我]',
    'Making: Wooden hatchet': '制作：木斧',
    'Next job': '下一步工作',
    'Shed': '小屋',
    'woodcap': '木上限',
    'This hatchet is great! Now I can clear paths in no time': '这把斧头真棒!现在我可以在任何时候清除路径',
    'The spaceship is going to crash land within the next few hours': '宇宙飞船将在几小时内坠毁',
    'You find the source of the beeping; it\'s an extremely old, dust covered monitor': '你找到了哔哔声的来源;这是一个非常旧的，布满灰尘的显示器',
    'There\'s nothing you can do but hold on for the ride...': '除了坐稳了，你别无他法……',
    'I\'ve landed. It wasn\'t as rough as I thought. However, my supplies are limited. I need to find some water': '我着陆了。它不像我想象中那么困难。但是，我的补给是有限的。在我的水用完之前(我需要每五小时喝一点水)，我需要快点找点水喝',
    'fast': '',
    ', before what I have runs out (I\'ll need to drink 1 water every five hours). Hopefully I can survive here...': '。希望我能在这里生存……',
    'Exploring: East': '探索:东',
    'Exploring: South': '探索:南',
    'Exploring: West': '探索:西',
    'Exploring: North': '探索:北',
    'Exploring: Plains': '探索:平原',
    'Exploring: Forest': '探索:森林',
    'Exploring: Desert': '探索:沙漠',
    'Exploring: Deep forest': '探索:森林深处',
    'After going to the north I discovered a large, sweeping plain. Will explore more later': '去了北方之后，我发现了一片广阔的平原。稍后将进一步探讨',
    'Go exploring in the desert': '去沙漠探险',
    'Hiked to the east, found a lake! Plenty of water now.': '向东走，发现了一个湖!现在有很多水。',
    'I found a bunch of trees to chop down. Well, what did I expect from a forest.': '我找到一堆树要砍。我对森林有什么期待?',
    'It looks like a desert down there... there won\'t be much water, unfortunately': '下面看起来像沙漠…很不幸，不会有很多水',
    'Went to the west, didn\'t find much. It appears there\'s a desert down south and a forest to the west.': '去了西部，没发现什么。看起来南边是沙漠，西边是森林。',
    'Put buckets on logs. Now you can carry more at once.': '把水桶放在木头上。现在你可以一次携带更多。',
    'Twin buckets': '两个桶',
    'Walking stick': '手杖',
    'Next job: Making: Even better WS': '下一项工作:制作:更好的手杖',
    'Rain collecter': '集雨器',
    'Making: Even better WS': '制作:更好的手杖',
    'This walking stick should enable me to explore even more faster': '这根手杖应该能让我更快地探索',
    'Last bought': '最近购买',
    'multiplier multiplies by 5 DOES NOT GUARANTEE a production increase of 5 - contact me if you really wanna know why.': '乘数乘以5不能保证产量增加5 -如果你真的想知道为什么联系我。',
    'Effect': '效果',
    'Water collecting ->': '收集水 ->',
    'Unlocked': '已解锁',
    'Small, medium woodcutting multiplier +.': '小型，中型木材加成+.',
    'Note': '注意',
    'Exploration speed +.': '探索速度+.',
    'Excellent WS': '优良的手杖',
    'Built a water bowl': '制作了一个水碗',
    'Building: Water bowl': '制作：水碗',
    'Bought': '已购买',
    'Better walking stick': '更好的手杖',
    'water': '水',
    'Next job: Larger woodcutting': '下一步工作：大量伐木',
    'wood': '木头',
    'Wood farm': '木头农场',
    'Wood farm  (': '木头农场  (',
    'Wooden tools': '木制工具',
    'This walking stick should enable me to explore even more fasterer': '这个手杖应该让我能够探索更多的紧固件',
    'Out here, there are some massive trees that I could use for wood.': '在这里，有一些可以用于木材的大树。',
    'Build a farm to farm wood': '建一个农场来种木头',
    'Built a rain collecter': '建一个雨水收集器',
    'Built a shed': '建一个棚子',
    'Built a wood farm': '建一个木头农场',
    'Collecting more leaves makes rain collectors faster and they can also store water': '收集更多的叶子使雨水收集器更快，他们也可以储存水',
    'I\'ll just go gather some leaves for my rain collecter...': '我会去收集一些叶子给我的雨水收集器......',
    'Leafier collecters': '叶子收集器',
    'Lighter big axes': '更轻大号的斧头',
    'Lighter medium axes': '更轻中号的斧头',
    'Lighter small axes': '更轻小号的斧头',
    'Make wooden tools to speed up constructions': '使木制工具加快建设速度',
    'Make your biggest axes lighter to reduce time to chop large trees': '让你的大号斧头更轻，以减少砍伐大树的时间',
    'Make your smalleset axes lighter to reduce time to chop small trees': '让你的中号斧头更轻，以减少砍伐大树的时间',
    'Make your smallest axes lighter to reduce time to chop medium trees': '让你的小号斧头更轻，以减少砍伐大树的时间',
    'Making: Lighter small axes': '制作：更轻小号的斧头',
    'Big woodcutting': '更大量伐木',
    'Cut down some of the big trees in the jungle': '砍下丛林中的一些大树',
    'Now with this upgrade, hunting will be faster': '现在通过这次升级，狩猎将更快',
    'These small axes are so light!': '这些小斧头很轻！',
    'Making: Wooden tools': '制作：木制工具',
    'Building: Shed': '建造：小屋',
    'Building: Wood farm': '建造：木头农场',
    'I guess workshop and building times should be decreased': '我想工作台和建筑时间应该减少',
    'Make wooden tools to speed up constructions more': '制作木制工具以加快施工速度',
    'More powerful wooden tools': '更强大的木制工具',
    'Next job: Big woodcutting': '下一个工作：更大伐木',
    'These large axes are so light!': '这些大型斧头太轻了！',
    'Making: Lighter medium axes': '制作：更轻的中型斧头',
    'Next job: Making: Lighter medium axes': '下一个工作：制作：更轻的中型斧头',
    'Cancel current job': '取消当前工作',
    'I\'ll just go and gather more leaves for my rain collectors..': '我会去为我的雨水收集器收集更多的叶子..',
    'Making: Leafier collecters': '制作：叶子收集者',
    'Making: More powerful wooden tools': '制作：更强大的木制工具',
    'Next job: Making: Leafier collecters': '下一个工作：制作：叶子收集者',
    'These medium axes are so light!': '这些中型斧头太轻了',
    'Big woodcutting time ->': '大伐木时间 - >',
    'Medium woodcutting speed ->': '中伐木速度 - >',
    'Medium woodcutting time ->': '中伐木时间 - >',
    'Woodcutting time ->': '伐木时间 ->',
    'Workshop speed ->': '工作台速度 ->',
    'rain collecter multiplier +=': '雨水收集器加成倍数 +=',
    'These tools are extremely powerful!': '这些工具非常强大！',
    'Deep desert': '沙漠深处',
    'Desert': '沙漠',
    'Exploring: Mountains': '探索: 高山',
    'Go exploring deep into the desert': '深入探索沙漠',
    'Go exploring the mountains': '去探索高山',
    'Mountains': '高山',
    'Next job: Making: More powerful wooden tools': '下一个工作：制作：更强大的木制工具',
    'Nothing to see in the desert except cactus and an oasis, got some water back.': '除了仙人掌和绿洲之外，在沙漠中看不到任何东西。',
    'Go exploring the mountain chain': '去探索山脉',
    'Climbed a mountain. There are so many more though!': '爬上一座山。 虽然还有很多！',
    'Hunting': '狩猎',
    'Go hunting on the plain': '在平原上打猎',
    'Go exploring the mountain chain': '去探索山脉',
    'Next job: Building: Bonfire': '下一份工作：建造：篝火',
    'Next job: Building: Fertiliser station': '下一份工作：建造：肥料站',
    'Next job: Building: Traps': '下一份工作：建造：陷阱',
    'Next job: Building: Water bowl': '下一份工作：建造：水碗',
    'North western plain': '西北平原',
    'Plain sheds': '平原棚屋',
    'Raw meat': '生肉',
    'rawmeat': '生肉',
    'rawmeatcap': '生肉上限',
    'Roast animal meat automatically': '自动烤动物肉',
    'See if there\'s an end to this unending plain.': '看看这无尽的平原是否有尽头。',
    'Traps': '陷阱',
    'Traps  (': '陷阱  (',
    'Traps animals to collect raw meat': '诱捕动物收集生肉',
    'I caught some weird animal. The meat seems edible-ish.': '我抓到一些奇怪的动物。这肉似乎可以吃。',
    'Invent the meatball sandwich! This gives great energy over long distances': '发明肉丸三明治!这给了长距离的巨大能量',
    'Invent the meatball snack! This gives jobs greater energy boosts': '发明肉丸子零食!这给就业带来了更大的能量提升',
    'Meatball sandwich': '肉丸三明治',
    'Meatball snack': '肉丸小吃',
    'Fertiliser station  (': '肥料站（',
    'Fertiliser station': '肥料站',
    'Exploring: More plains': '探索：更多平原',
    'Cooking': '烹饪',
    'cookedmeatcap': '熟肉上限',
    'Cooked meat': '熟肉',
    'Cook a piece of meat': '煮一块肉',
    'Build a fertiliser station. These boost effects of some other buildings (notably wood farms': '建一个肥料站。 对其他一些建筑物（特别是木材农场）起到促进作用',
    'Build larger sheds on the plains instead of at home': '在平原上建造更大的棚屋，而不是在家里',
    'Building: Fertiliser station': '建造：肥料站',
    'Built a bonfire': '建了一个篝火',
    'Built a traps': '建了一个陷阱',
    'C meat': '熟肉',
    'Bonfire': '篝火',
    'Building: Traps': '建造: 陷阱',
    'Convert': '转换',
    'rawmeat: -': '生肉: -',
    'wood: -': '木头: -',
    'cookedmeat': '煮肉',
    'Next job: Cooking': '下一份工作：烹饪',
    'Next job: Spear making': '下一份工作：制矛',
    'Making: Meatball snack': '制作：肉丸小吃',
    'Making: Plain sheds': '制作：平原棚屋',
    'Next job: Making: Meatball sandwich': '下一个工作：制作：肉丸三明治',
    'Next job: Making: Meatball snack': '下一份工作：制作：肉丸小吃',
    'Next job: Woodcutting': '下一份工作：伐木',
    'Next job: Making: Plain sheds': '下一份工作：制作：平原棚屋',
    'Built a fertiliser station': '建了一个肥料站',
    'Hiked out and set up a work area for making sheds': '徒步旅行，为搭建棚子设立了一个工作区',
    'Plain sheds amount increased': '平原棚物数量增加',
    'Almost all water things give each other a multiplicative boost': '几乎所有的水都会给对方带来倍增的推动力',
    'Axe amount increase': '斧量增加',
    'Bonfires coal gain is': '篝火煤的收益是',
    'Build a basic storage using wooden planks, for wood': '用木板搭建一个基本的储物空间',
    'Building speed =': '建造速度 =',
    'Carbon': '碳',
    'Caves': '洞穴',
    'Coal': '煤',
    'Coal bonfires': '煤炭篝火',
    'Coal mining': '采煤',
    'Coal torches': '煤炭火炬',
    'coalcap': '煤上限',
    'Condensed fertiliser': '浓缩肥料',
    'Steel': '钢',
    'Iron': '铁',
    'Graphite': '石墨',
    'Ore': '矿石',
    'Wooden planks': '木板',
    'W planks': '木板',
    'Big steel axes are stronger, lighter and faster': '大钢斧更坚固，更轻，更快',
    'Small steel axes are stronger, lighter and faster': '小钢斧更坚固，更轻，更快',
    'Steel big axes': '大钢斧',
    'Steel small axes': '小钢斧',
    'Iron bowls': '铁碗',
    'Iron buckets': '铁桶',
    'Iron hammers': '铁锤',
    'Iron pickaxes': '铁镐',
    'Iron sheds': '铁棚',
    'Iron stick': '铁棒',
    'Iron tipped axes': '铁尖斧',
    'Iron tipped spears': '铁尖矛',
    'Iron tools': '铁工具',
    'Iron/wood jets': '铁/木喉',
    'Water collecting =': '水收集 =',
    'carbon: -': '碳:-',
    'coal': '煤',
    'Furnace': '熔炉',
    'iron: -': '铁: -',
    'One of the only storages possible for pure carbon': '这是唯一可能储存纯碳的方法之一',
    'Ranger station  (': '管理站(',
    'steel': '钢',
    'Stock up a station far out in the forests to provide a variety of benefits': '在森林里建一个车站，提供各种各样的好处',
    'Divert water into a rock surface, thus hammering away at it and extracting ores': '把水引到岩石表面，从而锤打岩石并提取矿石',
    'Carbon storage  (': '碳储存（',
    'Convert ores to the minerals': '将矿石转换为矿物质',
    'Craft steel from iron and coal': '由铁和煤制成钢',
    'graphitecap': '石墨上限',
    'orecap': '矿石上限',
    'ironcap': '铁上限',
    'Plankstorage  (': '板材存储  (',
    'Smelt almost anything carbon-based to pure carbon': '把几乎所有以碳为基础的东西都熔炼成纯碳',
    'graphite': '石墨',
    'graphite: -': '石墨：-',
    'iron': '铁',
    'ore': '矿石',
    'ore: -': '矿石: -',
    'Plankstorage': '板材存储',
    'Ranger station': '管理站',
    'Smelter': '冶炼厂',
    'water: -': '水: -',
    'Waterstream': '水流',
    'exploration water divider +.': '勘探分水器+.',
    'Explore speed +': '探索速度+',
    'exporation speed +.': '出口速度+.',
    'Fertiliser stations wood farm boost is 5x more effective': '化肥站、木材农场的增产效果是原来的5倍',
    'Fish traps': '捕鱼器',
    'global jobs +.': '全局工作+.',
    'Graphite smelters': '石墨冶炼厂',
    'Hunting multiplier +': '狩猎加成倍数+',
    'Medium steel axes are stronger, lighter and faster': '中型钢斧更强，更轻，更快',
    'Unlock wooden planks': '解锁模板',
    'Unlock Mines (exploration': '解锁矿山(探索',
    'Steel sheds': '钢棚',
    'Steel tools': '钢工具',
    'Torchholders': '火炬手',
    'Salami bite': '咬痕',
    'Reinforced sheds': '加强棚',
    'Salami burger': '腊肠汉堡',
    'Shed storage amount increased': '棚存量增加',
    'Sheds multiplier +': '棚子加成倍数 +',
    'Sheds store more stuff': '棚子存放更多东西',
    'Water collectors\' multiplier grows quadratically w.r.t number of waterbowls': '水收集器的倍增器以水柱数量的方式平均增长',
    'Waterbowl collecters': '水碗收集器',
    'Wooden pickaxe': '木镐',
    'Workshop speed =': '工作台速度 =',
    'Workshop speed multiplier ->': '工作台速度加成倍数 - >',
    'ore (job) time -': '矿石 (工作) 时间 -',
    'More exploration speed': '更快的探索速度',
    'mining ore amount +': '开采矿石量 +',
    'Ore (job) time -': '矿石 (工作) 时间 -',
    'Unlock woodplank sheds': '解锁木板棚',
    'Traps multiplier +': '陷阱加成倍数 +',
    'Steel medium axes': '中型钢斧',
    'Smelter piping': '冶炼厂管道',
    'smelter graphite +': '冶炼石墨 +',
    'Smelter amount increased': '冶炼厂数量增加',
    'ore (job) multiplier +': '矿石 (工作) 加成 -',
    'Chop wood into wooden planks': '将木头切成木板',
    'Medium woodc. m': '中型伐木',
    'Medium woodc. time ->': '中型伐木 时间 ->',
    'Mine some coals': '挖一些煤',
    'Mine some ores': '挖一些矿石',
    'Mining': '采矿',
    'Saw wooden planks': '锯木板',
    'Next job: Coal mining': '下一个工作：煤炭开采',
    'Explore the caves a bit more': '探索洞穴多一点',
    'Fishery': '渔业',
    'Go exploring deep into the cave system': '深入探索洞穴系统',
    'Go exploring into the cave system': '去探索洞穴系统',
    'Go exploring the mountain range': '去探索山脉',
    'Mines': '矿业',
    'Set up a mine here in the shiny stuff': '在这闪闪发光的地方挖个矿',
    'Search for fishing places around the lake': '搜索湖周围的钓鱼场所',
    'Refresh to complete. Alternatively, click save from the menu if you wish to undo': '刷新完成。或者，如果您希望撤消操作，可以从菜单中点击保存',
    'Are you sure?': '你确定？',
    'Bait fishies with meat to get more meat': '用肉做鱼饵以获得更多的肉',
    'Build crude piping for smelters, thus improving smelter efficiency': '为冶炼厂建造原油管道，从而提高冶炼厂的效率',
    'Infuse axes with iron; this will make axes last longer and be stronger': '向斧子中注入铁;这将使斧头更耐用，更坚固',
    'Infuse building tools with iron; make building buildings faster': '用铁注入建筑工具;加快建筑速度',
    'Infuse pickaxes with iron; this will allow them to be sturdier and stronger': '用铁注入镐;这将使他们更加坚强',
    'Infuse some iron into those sheds to bump up storage amount (sheds will not cost iron': '在这些棚子里注入一些铁来增加储存量(棚子不需要铁',
    'Infuse workshop tools with iron; make tools faster': '在工作台的工具中注入铁;使工具制作更快',
    'Make an iron tipped walking stick to walk further, faster': '做一根带铁尖的手杖，走得更远、更快',
    'Make buckets out of iron; this will make them larger and carry more': '用铁做桶;这将使他们更大，携带更多',
    'Make iron spears to take down bigger game': '制作铁制长矛来对付更大的猎物',
    'Next job: Exploring: Mountains': '下一项工作:探索:山脉',
    'Next job: Hunting': '下一份工作:狩猎',
    'Next job: Mining': '下一份工作:采矿',
    'Next job: Water collecting': '下一项工作:集水',
    'Next job: Making: Better walking stick': '下一个工作：制作：更好的手杖',
    'Next job: Making: Fish traps': '下一个工作：制作：鱼陷阱',
    'Next job: Making: Iron buckets': '下一个工作：制作：铁桶',
    'Next job: Making: Iron hammers': '下一个工作：制作：铁锤',
    'Next job: Making: Iron pickaxes': '下一个工作：制作：铁镐',
    'Next job: Making: Iron sheds': '下一份工作：制作：铁棚',
    'Next job: Making: Iron stick': '下一个工作：制作：铁棒',
    'Next job: Making: Iron tipped axes': '下一项工作:制造:铁尖斧头',
    'Next job: Making: Iron tipped spears': '下一项工作:制造:铁头矛',
    'Next job: Making: Iron tools': '下一项工作:制造:铁制工具',
    'Next job: Making: Lightweight spears': '下一项工作:制作:轻量的长矛',
    'Making: Meatball sandwich': '制作:肉丸三明治',
    'Building: Bonfire': '建造：篝火',
    'Building: Carbon purifier': '建造：碳净化器',
    'Building: Carbon storage': '建造：碳储存',
    'Building: Ranger station': '建造:管理站',
    'Building: Smelter': '建造:冶炼厂',
    'Building: Waterstream': '建造:水流',
    'Carbon purifier': '碳净化器',
    'Carbon storage': '碳储存',
    'Next job: Building: Carbon purifier': '下一个工作：建造：碳净化器',
    'Next job: Building: Carbon storage': '下一份工作：建造：碳储存',
    'Next job: Building: Rain collecter': '下一份工作:建造:雨水收集器',
    'Next job: Building: Ranger station': '下一项工作:建造:护林员站',
    'Next job: Building: Shed': '下一项工作:建造:棚屋',
    'Next job: Building: Smelter': '下一项工作:建造:冶炼厂',
    'Next job: Building: Waterstream': '工作:建造:水流',
    'Next job: Building: Wood farm': '下一项工作:建造:木材农场',
    'Making: Fish traps': '制作：鱼陷阱',
    'Making: Iron buckets': '制作：铁桶',
    'Making: Iron pickaxes': '制作：铁镐',
    'Making: Iron stick': '制作：铁棒',
    'Making: Iron tools': '制作：铁制工具',
    'Coal torches last longer than before': '煤炬的使用寿命比以前的时间长。',
    'Extract coal from bonfires with new iron pokers': '用新的铁锥从篝火中提取煤',
    'Teach your smelters to extract graphite': '教你的冶炼厂提炼石墨',
    'Climbed another bunch of mountains; seems to be nothing much ahead except taller and taller mountains': '爬上另一堆山; 除了更高更高的山脉，似乎没有什么比这更好的了',
    'Making: Coal bonfires': '制作：煤炭篝火',
    'Making: Coal torches': '制作：煤火炬',
    'Making: Graphite smelters': '制作：石墨冶炼厂',
    'Next job: Making: Coal bonfires': '下一份工作：制作：煤炭篝火',
    'Next job: Making: Coal torches': '下一个工作：制作：煤火炬',
    'Next job: Making: Graphite smelters': '下一个工作：制作：石墨冶炼厂',
    'Building: Rain collecter': '建造：雨水收集器',
    'Make a pickaxe to mine some of those ores from the mine': '做一个镐从矿井中采集一些这些矿石',
    'Making: Better walking stick': '制作：更好的手杖',
    'Making: Iron hammers': '制作：铁锤',
    'Making: Iron sheds': '制作：铁棚',
    'Making: Iron tipped spears': '制作：铁尖矛',
    'Making: Lightweight spears': '制作：轻量级长矛',
    'This walking stick should enable me to explore even faster': '这个手杖应该可以让我更快地探索',
    'Add a network of iron and wood pipes to connect all water things together': '添加铁和木管网络，将所有水混合在一起',
    'Boost water collectors substantially': '大大提高了水收集器',
    'Infuse bowls with iron to give an additive boost to rain collectors': '用铁注入碗以增加雨水收集器',
    'Invent salami bites! These morsels can give massive energy': '发明萨拉米香肠！ 这些食物可以提供巨大的能量',
    'Invent the salami burger! This gives massive energy over increased distances': '发明萨拉米汉堡！ 这会在距离增加时产生巨大的能量',
    'Making: Steel big axes': '制作：钢制大斧',
    'Making: Steel small axes': '制作：钢制小斧',
    'Next job: Making: Steel big axes': '下一个工作：制作：钢制大斧',
    'Next job: Making: Steel small axes': '下一个工作：制作：钢制小斧',
    'Steel sheds are much sturdier and therefore larger': '钢棚更坚固，因此更大',
    'Steel tools boost workshops a bit more': '钢铁工具可以进一步推动研讨会',
    'Torch holders free both hands for mining': '火炬手可以双手采矿',
    'Use wooden planks to gain extra space for sheds': '使用木板为棚屋腾出更多空间',
    'Vastly improve the effect of fertiliser stations on wood farms': '大大提高化肥站对林场的影响',
    'carbon': '碳',
    'carboncap': '碳上限',
    'coal: -': '煤: -',
    'coal: 0.00..': '煤: 0.00..',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    '': '',
    'v0 a new game': 'v0 一个新游戏',
    '.5 added a few more upgrades to boost progress': '.5 增加了一些升级来提高进度',
    '.2 workshop upgrades seeable': '.2 工作台升级可见',
    '.2 \'a bunch of upgrades\'': '.2 “一系列升级”',
    '.0 the basics': '.0 基本内容',

    //原样
    '': '',
    '': '',

}

//需处理的前缀
var cnPrefix = {
    "(-": "(-",
    "(+": "(+",
    "(": "(",
    "-": "-",
    "+": "+",
    " ": " ",
    ": ": "： ",
}

//需处理的后缀
var cnPostfix = {
    ":": "：",
    "：": "：",
    ": ": "： ",
    "： ": "： ",
    " ": "",
    "/s)": "/s)",
    "/s": "/s",
    ")": ")",
    "%": "%",
}

//需排除的，正则匹配
var cnExcludeWhole = [
    /^x?\d+(\.\d+)?[A-Za-z%]{0,2}(\s.C)?\s*$/, //12.34K,23.4 °C
    /^x?\d+(\.\d+)?(e[+\-]?\d+)?\s*$/, //12.34e+4
    /^\s*$/, //纯空格
    /^\d+(\.\d+)?[A-Za-z]{0,2}.?\(?([+\-]?(\d+(\.\d+)?[A-Za-z]{0,2})?)?$/, //12.34M (+34.34K
    /^(\d+(\.\d+)?[A-Za-z]{0,2}\/s)?.?\(?([+\-]?\d+(\.\d+)?[A-Za-z]{0,2})?\/s\stot$/, //2.74M/s (112.4K/s tot
    /^\d+(\.\d+)?(e[+\-]?\d+)?.?\(?([+\-]?(\d+(\.\d+)?(e[+\-]?\d+)?)?)?$/, //2.177e+6 (+4.01+4
    /^(\d+(\.\d+)?(e[+\-]?\d+)?\/s)?.?\(?([+\-]?(\d+(\.\d+)?(e[+\-]?\d+)?)?)?\/s\stot$/, //2.177e+6/s (+4.01+4/s tot
];
var cnExcludePostfix = [
    /:?\s*x?\d+(\.\d+)?(e[+\-]?\d+)?\s*$/, //12.34e+4
    /:?\s*x?\d+(\.\d+)?[A-Za-z]{0,2}$/, //: 12.34K, x1.5
]

//正则替换，带数字的固定格式句子
var cnRegReplace = new Map([
	[/^Day (\d+),$/, '$1 天，'],
	[/^Bonfire  \((\d+)\/$/, '篝火 \($1 \/'],
    [/^Furnace  \((\d+)\/$/, '熔炉 \($1 \/'],
    [/^Smelter  \((\d+)\/$/, '冶炼厂 \($1 \/'],
    [/^Waterstream  \((\d+)\/$/, '水流 \($1 \/'],
	[/^Carbon purifier  \((\d+)\/$/, '碳净化器 \($1 \/'],
	[/^Cost: (\d+) RP$/, '成本：$1 皇家点数'],
	[/^Usages: (\d+)\/$/, '用途：$1\/'],
	[/^workers: (\d+)\/$/, '工人：$1\/'],

]);

//2.采集新词
//20190320@JAR

var cnItem = function () {

    //传参是否非空字串
    if (!arguments[0]) return "";

    //检验传参是否对象
    let text = arguments[0],
        s = '';
    if (typeof (text) != "string")
        return text;
    else
        s = arguments[0].charCodeAt();

    //检验传参是否英文
    // if (
    //     s < 65 || (s > 90 && s < 97) || (s > 122)
    //
    // ) return text;

    //处理前缀
    let text_prefix = "";
    for (let prefix in cnPrefix) {
        if (text.substr(0, prefix.length) === prefix) {
            text_prefix = cnPrefix[prefix];
            text = text.substr(prefix.length);
        }
    }
    //处理后缀
    let text_postfix = "";
    for (let postfix in cnPostfix) {
        if (text.substr(-postfix.length) === postfix) {
            text_postfix = cnPostfix[postfix];
            text = text.substr(0, text.length - postfix.length);
        }
    }
    //处理正则后缀
    let text_reg_exclude_postfix = "";
    for (let reg of cnExcludePostfix) {
        let result = text.match(reg);
        if (result) {
            text_reg_exclude_postfix = result[0];
            text = text.substr(0, text.length - text_reg_exclude_postfix.length);
        }
    }

    //检验字典是否可存
    if (!cnItems._OTHER_) cnItems._OTHER_ = [];

    //检查是否排除
    for (let reg of cnExcludeWhole) {
        if (reg.test(text)) {
            return arguments[0];
        }
    }

    //尝试正则替换
    for (let [key, value] of cnRegReplace.entries()) {
        if (key.test(text)) {
            return text_prefix + text.replace(key, value) + text_reg_exclude_postfix + text_postfix;
        }
    }

    //遍历尝试匹配
    for (let i in cnItems) {
        //字典已有词汇或译文、且译文不为空，则返回译文
        if (
            text == i || text == cnItems[i] &&
            cnItems[i] != ''
        )
            return text_prefix + cnItems[i] + text_reg_exclude_postfix + text_postfix;
    }

    //调整收录的词条，0=收录原文，1=收录去除前后缀的文本
    let save_cfg = 1;
    let save_text = save_cfg ? text : arguments[0]
    //遍历生词表是否收录
    for (
        let i = 0; i < cnItems._OTHER_.length; i++
    ) {
        //已收录则直接返回
        if (save_text == cnItems._OTHER_[i])
            return arguments[0];
    }

    if (cnItems._OTHER_.length < 500) {
        //未收录则保存
        cnItems._OTHER_.push(save_text);
        cnItems._OTHER_.sort(
            function (a, b) {
                return a.localeCompare(b)
            }
        );
    }

    /*
        //开启生词打印
        //console.log(
            '有需要汉化的英文：', text
        );
    */

    //返回生词字串
    return arguments[0];
};

transTaskMgr = {
    tasks: [],
    addTask: function (node, attr, text) {
        this.tasks.push({
            node,
            attr,
            text
        })
    },
    doTask: function () {
        let task = null;
        while (task = this.tasks.pop())
            task.node[task.attr] = task.text;
    },
}

function TransSubTextNode(node) {
    if (node.childNodes.length > 0) {
        for (let subnode of node.childNodes) {
            if (subnode.nodeName === "#text") {
                let text = subnode.textContent;
                let cnText = cnItem(text);
                cnText !== text && transTaskMgr.addTask(subnode, 'textContent', cnText);
                //console.log(subnode);
            } else if (subnode.nodeName !== "SCRIPT" && subnode.nodeName !== "TEXTAREA") {
                if (!subnode.childNodes || subnode.childNodes.length == 0) {
                    let text = subnode.innerText;
                    let cnText = cnItem(text);
                    cnText !== text && transTaskMgr.addTask(subnode, 'innerText', cnText);
                    //console.log(subnode);
                } else {
                    TransSubTextNode(subnode);
                }
            } else {
                // do nothing;
            }
        }
    }
}

! function () {
    console.log("加载汉化模块");

    let observer_config = {
        attributes: false,
        characterData: true,
        childList: true,
        subtree: true
    };
    let targetNode = document.body;
    //汉化静态页面内容
    TransSubTextNode(targetNode);
    transTaskMgr.doTask();
    //监听页面变化并汉化动态内容
    let observer = new MutationObserver(function (e) {
        //window.beforeTransTime = performance.now();
        observer.disconnect();
        for (let mutation of e) {
            if (mutation.target.nodeName === "SCRIPT" || mutation.target.nodeName === "TEXTAREA") continue;
            if (!mutation.target.childNodes || mutation.target.childNodes.length == 0) {
                mutation.target.innerText = cnItem(mutation.target.innerText);
            } else if (mutation.target.nodeName === "#text") {
                mutation.target.textContent = cnItem(mutation.target.textContent);
            } else if (mutation.addedNodes.length > 0) {
                for (let node of mutation.addedNodes) {
                    if (node.nodeName === "#text") {
                        node.textContent = cnItem(node.textContent);
                        //console.log(node);
                    } else if (node.nodeName !== "SCRIPT" && node.nodeName !== "TEXTAREA") {
                        if (!node.childNodes || node.childNodes.length == 0) {
                            node.innerText = cnItem(node.innerText);
                        } else {
                            TransSubTextNode(node);
                            transTaskMgr.doTask();
                        }
                    }
                }
            }
        }
        observer.observe(targetNode, observer_config);
        //window.afterTransTime = performance.now();
        //console.log("捕获到页面变化并执行汉化，耗时" + (afterTransTime - beforeTransTime) + "毫秒");
    });
    observer.observe(targetNode, observer_config);
}();
